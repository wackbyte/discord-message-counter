use {
    rayon::prelude::*,
    std::{
        fs::{DirEntry, File},
        io::{self, BufRead, BufReader},
        path::PathBuf,
    },
    structopt::StructOpt,
};

/// a tool to count how many messages you have in discord
#[derive(StructOpt, Debug, Clone)]
struct Opt {
    /// the path to a discord data package (that has already been extracted)
    package: PathBuf,
}

/// print an error and exit with an error code
macro_rules! oops {
    ($($arg:tt)*) => {
        {
            eprintln!($($arg)*);
            std::process::exit(1);
        }
    };
}

fn main() {
    let Opt { package } = Opt::from_args();

    if package.is_dir() {
        let messages = package.join("messages");
        if messages.is_dir() {
            match messages.read_dir() {
                Ok(chunks) => match chunks.collect::<Result<Vec<DirEntry>, io::Error>>() {
                    Ok(chunks) => {
                        // count each message in parallel
                        let counter = chunks
                            .par_iter()
                            .map(|entry| {
                                let chunk = entry.path();

                                // the messages folder only contains more folders EXCEPT for index.json
                                // so we just return 0 if we get that
                                if chunk.is_dir() {
                                    let data = chunk.join("messages.csv");
                                    if data.is_file() {
                                        match File::open(data.clone()) {
                                            Ok(file) => {
                                                // wrap the file in a BufReader so we can count the lines easily
                                                let reader = BufReader::new(file);
                                                let lines = reader.lines().count();

                                                // messages.csv files start with a header line that labels each column,
                                                // so we can just subtract 1 from the line count
                                                // and get the amount of messages in the chunk!
                                                //
                                                // if it's invalid and doesn't have any lines then throw an error
                                                if let Some(lines) = (lines as u64).checked_sub(1) {
                                                    lines
                                                } else {
                                                    oops!(
                                                        "invalid messages file at `{:?}` is empty",
                                                        data
                                                    )
                                                }
                                            }
                                            Err(error) => {
                                                oops!("could not read `{:?}`: {}", data, error);
                                            }
                                        }
                                    } else {
                                        oops!("`{:?}` is not a file", data);
                                    }
                                } else {
                                    0
                                }
                            })
                            .sum::<u64>();

                        // if we're outputting to a tty then put a line on the end of the output
                        // idk if i'm supposed to do this but it seems right?
                        if atty::is(atty::Stream::Stdout) {
                            println!("{}", counter);
                        } else {
                            print!("{}", counter);
                        }
                    }
                    Err(error) => {
                        oops!(
                            "could not read messages chunk in `{:?}`: {}",
                            messages,
                            error
                        );
                    }
                },
                Err(error) => {
                    oops!("could not read `{:?}`: {}", messages, error);
                }
            }
        } else {
            oops!("`{:?}` is not a directory", messages);
        }
    } else {
        oops!("`{:?}` is not a directory", package);
    }
}
